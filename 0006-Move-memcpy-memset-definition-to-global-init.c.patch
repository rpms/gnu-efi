From b1d426ce67cbeaa1a8ec7d9b8d0d8071ae4563b4 Mon Sep 17 00:00:00 2001
From: Nigel Croxon <ncroxon@redhat.com>
Date: Mon, 6 Nov 2017 09:38:38 -0500
Subject: [PATCH 06/25] Move memcpy/memset definition to global init.c

Following up on previous patch, I think we should move
memcpy/memset definitions to the global init.c, since MSVC does
also inserts calls to memset/memcpy for the x86_32 platform,
even when disabling standard libraries and intrinsics.

All in all, it looks like, for all platforms, we should assume
that a compiler may still insert these calls regardless.

Signed-off-by: Pete Batard <pete@akeo.ie>
Signed-off-by: Nigel Croxon <ncroxon@redhat.com>
---
 lib/aarch64/initplat.c  | 29 -----------------------------
 lib/arm/initplat.c      | 33 ++-------------------------------
 lib/init.c              | 47 ++++++++++++++++++++++++++++++++++++++---------
 lib/mips64el/initplat.c | 25 -------------------------
 4 files changed, 40 insertions(+), 94 deletions(-)

diff --git a/lib/aarch64/initplat.c b/lib/aarch64/initplat.c
index 25207f42c8f..6c5e1fa5217 100644
--- a/lib/aarch64/initplat.c
+++ b/lib/aarch64/initplat.c
@@ -24,32 +24,3 @@ InitializeLibPlatform (
     )
 {
 }
-
-#ifndef __SIZE_TYPE__
-#define __SIZE_TYPE__ UINTN
-#endif
-
-/*
- * Calls to these functions may be emitted implicitly by GCC even when
- * -ffreestanding is in effect.
- */
-void *memset(void *s, int c, __SIZE_TYPE__ n)
-{
-    unsigned char *p = s;
-
-    while (n--)
-        *p++ = c;
-
-    return s;
-}
-
-void *memcpy(void *dest, const void *src, __SIZE_TYPE__ n)
-{
-    const unsigned char *q = src;
-    unsigned char *p = dest;
-
-    while (n--)
-        *p++ = *q++;
-
-    return dest;
-}
diff --git a/lib/arm/initplat.c b/lib/arm/initplat.c
index c77d26d9045..135a649210a 100644
--- a/lib/arm/initplat.c
+++ b/lib/arm/initplat.c
@@ -25,39 +25,10 @@ InitializeLibPlatform (
 {
 }
 
-#ifndef __SIZE_TYPE__
-#define __SIZE_TYPE__ UINTN
-#endif
-
-/*
- * Calls to these functions may be emitted implicitly by GCC even when
- * -ffreestanding is in effect.
- */
-void *memset(void *s, int c, __SIZE_TYPE__ n)
-{
-    unsigned char *p = s;
-
-    while (n--)
-        *p++ = c;
-
-    return s;
-}
-
-void *memcpy(void *dest, const void *src, __SIZE_TYPE__ n)
-{
-    unsigned char *p = dest;
-    unsigned char const *q = src;
-
-    while (n--)
-        *p++ = *q++;
-
-    return dest;
-}
-
 #ifdef __GNUC__
 void __div0(void)
 {
-	// TODO handle divide by zero fault
-	while (1);
+    // TODO handle divide by zero fault
+    while (1);
 }
 #endif
diff --git a/lib/init.c b/lib/init.c
index 4225d314b7a..4f238c0a2cc 100644
--- a/lib/init.c
+++ b/lib/init.c
@@ -49,7 +49,7 @@ Returns:
     if (!LibInitialized) {
         LibInitialized = TRUE;
         LibFwInstance = FALSE;
-	LibImageHandle = ImageHandle;
+        LibImageHandle = ImageHandle;
 
 
         //
@@ -71,17 +71,16 @@ Returns:
 
         if (ImageHandle) {
             Status = uefi_call_wrapper(
-			    BS->HandleProtocol,
-				3,
-                            ImageHandle, 
-                            &LoadedImageProtocol,
-                            (VOID*)&LoadedImage
-                            );
+                BS->HandleProtocol,
+                3,
+                ImageHandle, 
+                &LoadedImageProtocol,
+                (VOID*)&LoadedImage
+            );
 
             if (!EFI_ERROR(Status)) {
                 PoolAllocationType = LoadedImage->ImageDataType;
             }
-            
             EFIDebugVariable ();
         }
 
@@ -181,5 +180,35 @@ EFIDebugVariable (
     Status = uefi_call_wrapper(RT->GetVariable, 5, L"EFIDebug", &EfiGlobalVariable, &Attributes, &DataSize, &NewEFIDebug);
     if (!EFI_ERROR(Status)) {
         EFIDebug = NewEFIDebug;
-    } 
+    }
+}
+
+/*
+ * Calls to memset/memcpy may be emitted implicitly by GCC or MSVC
+ * even when -ffreestanding or /NODEFAULTLIB are in effect.
+ */
+
+#ifndef __SIZE_TYPE__
+#define __SIZE_TYPE__ UINTN
+#endif
+
+void *memset(void *s, int c, __SIZE_TYPE__ n)
+{
+    unsigned char *p = s;
+
+    while (n--)
+        *p++ = c;
+
+    return s;
+}
+
+void *memcpy(void *dest, const void *src, __SIZE_TYPE__ n)
+{
+    const unsigned char *q = src;
+    unsigned char *p = dest;
+
+    while (n--)
+        *p++ = *q++;
+
+    return dest;
 }
diff --git a/lib/mips64el/initplat.c b/lib/mips64el/initplat.c
index 4b118656d7c..6c5e1fa5217 100644
--- a/lib/mips64el/initplat.c
+++ b/lib/mips64el/initplat.c
@@ -24,28 +24,3 @@ InitializeLibPlatform (
     )
 {
 }
-
-/*
- * Calls to these functions may be emitted implicitly by GCC even when
- * -ffreestanding is in effect.
- */
-void *memset(void *s, int c, __SIZE_TYPE__ n)
-{
-    unsigned char *p = s;
-
-    while (n--)
-        *p++ = c;
-
-    return s;
-}
-
-void *memcpy(void *dest, const void *src, __SIZE_TYPE__ n)
-{
-    const unsigned char *q = src;
-    unsigned char *p = dest;
-
-    while (n--)
-        *p++ = *q++;
-
-    return dest;
-}
-- 
2.15.0

